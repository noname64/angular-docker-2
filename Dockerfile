FROM node:10.19 as builder
WORKDIR /app
COPY package.json package.json
RUN npm install
COPY . .
RUN npm run build -- --prod

FROM nginx:alpine

COPY ./config/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/dist/test-angular-multi /usr/share/nginx/html



